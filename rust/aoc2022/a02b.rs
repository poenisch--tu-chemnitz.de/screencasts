/// Advent of Code 2022, Tag 2, Teil 2
/// www.adventofcode.com/2022/
use std::io;

#[derive(Debug)]
enum Action {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug)]
enum Objective {
    Lose,
    Draw,
    Win,
}

fn str_to_action(s: &str) -> Action {
    match s {
        "A" => Action::Rock,
        "B" => Action::Paper,
        "C" => Action::Scissors,
        &_ => Action::Rock, // tritt nicht auf, sonst Action::None verwenden!
    }
}

fn str_to_objective(s: &str) -> Objective {
    match s {
        "X" => Objective::Lose,
        "Y" => Objective::Draw,
        "Z" => Objective::Win,
        &_ => Objective::Lose, // tritt nicht auf, sonst Action::None verwenden!
    }
}

fn action_value(a: Action) -> i32 {
    match a {
        Action::Rock => 1,
        Action::Paper => 2,
        Action::Scissors => 3,
    }
}

fn objective_value(o: Objective) -> i32 {
    match o {
        Objective::Lose => 0,
        Objective::Draw => 3,
        Objective::Win => 6,
    }
}

fn play(action_a: Action, objective_b: Objective) -> i32 {
    let action_b = match action_a {
        Action::Rock => match objective_b {
            Objective::Lose => Action::Scissors,
            Objective::Draw => Action::Rock,
            Objective::Win => Action::Paper,
        },
        Action::Paper => match objective_b {
            Objective::Lose => Action::Rock,
            Objective::Draw => Action::Paper,
            Objective::Win => Action::Scissors,
        },
        Action::Scissors => match objective_b {
            Objective::Lose => Action::Paper,
            Objective::Draw => Action::Scissors,
            Objective::Win => Action::Rock,
        },
    };
    objective_value(objective_b) + action_value(action_b)
}

fn main() {
    let mut inp = String::new();
    let mut score = 0;
    loop {
        inp.clear();
        let bytes = io::stdin().read_line(&mut inp).expect("Error reading line");
        if bytes == 0 {
            break;
        }
        let mut action_a = Action::Rock; // wird überschrieben!
        let mut objective_b = Objective::Lose;
        let mut i = 0;
        for s in inp.trim().split(" ") {
            if i == 0 {
                action_a = str_to_action(s);
            } else {
                objective_b = str_to_objective(s);
            }
            i += 1;
        }
        println!("{:?} {:?}", action_a, objective_b);
        let score_round = play(action_a, objective_b);
        println!("Score {score_round}");
        score += score_round;
    }
    println!("Total score: {score}", score = score);
}
