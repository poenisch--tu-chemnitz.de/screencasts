use std::io;

fn main() {
    let mut elves = Vec::new();
    loop {
        let elf = read_elf();
        if elf < 0 {
            break;
        }
        elves.push(elf);
    }
    println!("{elves:?}");
    let cal_max = get_max(&elves);
    println!("Max: {cal_max}");
    elves.sort();
    println!("{elves:?}");
    let cal_max_3 = sum_last(&elves, 3);
    println!("Sum of 3 max: {cal_max_3}");
}

fn read_elf() -> i32 {
    let mut inp = String::new();
    let mut sum = 0;
    loop {
        inp.clear();
        let bytes = io::stdin().read_line(&mut inp).expect("not readable");
        if bytes == 0 {
            if sum > 0 {
                return sum;
            } else {
                return -1;
            }
        }
        let inp = inp.trim();
        if inp == "" {
            return sum;
        }
        sum += inp.parse::<i32>().expect("not a number");
    }
}

fn get_max(elves: &Vec<i32>) -> i32 {
    let mut max = elves[0];
    for elf in elves {
        if *elf > max {
            max = *elf;
        }
    }
    max
}

fn sum_last(elves: &Vec<i32>, k: usize) -> i32 {
    let mut sum = 0;
    let idx_end = elves.len();
    for i in 1..=k {
        //println!("{} {} {}", i, idx_end, idx_end - i);
        sum += elves[idx_end - i];
    }
    sum
}
