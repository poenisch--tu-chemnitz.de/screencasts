/// Advent of Code 2022, Tag 2, Teil 1
/// www.adventofcode.com/2022/
use std::io;

#[derive(Debug)]
enum Action {
    Rock,
    Paper,
    Scissors,
}

fn str_to_action(s: &str) -> Action {
    match s {
        "A" | "X" => Action::Rock,
        "B" | "Y" => Action::Paper,
        "C" | "Z" => Action::Scissors,
        &_ => Action::Rock, // tritt nicht auf, sonst Action::None verwenden!
    }
}

fn action_value(a: Action) -> i32 {
    match a {
        Action::Rock => 1,
        Action::Paper => 2,
        Action::Scissors => 3,
    }
}

fn play(a: Action, b: Action) -> i32 {
    let res = match a {
        Action::Rock => match b {
            Action::Rock => 3,
            Action::Paper => 6,
            Action::Scissors => 0,
        },
        Action::Paper => match b {
            Action::Rock => 0,
            Action::Paper => 3,
            Action::Scissors => 6,
        },
        Action::Scissors => match b {
            Action::Rock => 6,
            Action::Paper => 0,
            Action::Scissors => 3,
        },
    };
    res + action_value(b)
}

fn main() {
    let mut inp = String::new();
    let mut score = 0;
    loop {
        inp.clear();
        let bytes = io::stdin().read_line(&mut inp).expect("Error reading line");
        if bytes == 0 {
            break;
        }
        let mut action_a = Action::Rock; // wird überschrieben!
        let mut action_b = Action::Rock;
        let mut i = 0;
        for s in inp.trim().split(" ") {
            if i == 0 {
                action_a = str_to_action(s);
            } else {
                action_b = str_to_action(s);
            }
            i += 1;
        }
        println!("{:?} {:?}", action_a, action_b);
        let score_round = play(action_a, action_b);
        println!("Score {score_round}");
        score += score_round;
    }
    println!("Total score: {score}", score = score);
}
