use std::io;

fn add_to_stacks(stacks: &mut Vec<Vec<u8>>, line: &str) -> bool {
    let bytes = line.as_bytes();
    let mut idx = 0;
    let mut changed = false;
    while idx < bytes.len() {
        if bytes[idx] == b'[' {
            let stack_nr = idx / 4 + 1;
            stacks[stack_nr].insert(0, bytes[idx + 1]);
            changed = true;
        }
        idx += 4;
    }
    changed
}

fn show_stacks(stacks: &Vec<Vec<u8>>) {
    for i in 1..10 {
        print!("{i}: ");
        for c in &stacks[i] {
            print!("{}", *c as char);
        }
        println!();
    }
}

fn parse_move(line: &str) -> (usize, usize, usize) {
    let parts: Vec<&str> = line.split(' ').collect();
    let count: usize = parts[1].parse().unwrap();
    let from: usize = parts[3].parse().unwrap();
    let to: usize = parts[5].parse().unwrap();
    (count, from, to)
}

fn build_solution(stacks: &Vec<Vec<u8>>) -> String {
    let mut res = String::new();
    for i in 1..10 {
        if let Some(val) = stacks[i].last() {
            res.push(*val as char);
        } else {
            res.push('?');
        }
    }
    res
}

// Idee: Hilfsstapel
// A B C (move 2) C B (move 2) B C
#[allow(dead_code)]
fn move_stacks1(stacks: &mut Vec<Vec<u8>>, count: usize, from: usize, to: usize) {
    let mut help: Vec<u8> = Vec::new();
    for _ in 0..count {
        if let Some(val) = stacks[from].pop() {
            help.push(val);
        } else {
            help.push(b'?');
        }
    }
    for _ in 0..count {
        if let Some(val) = help.pop() {
            stacks[to].push(val);
        } else {
            stacks[to].push(b'?');
        }
    }
}

#[allow(dead_code)]
fn move_stacks2(stacks: &mut Vec<Vec<u8>>, count: usize, from: usize, to: usize) {
    let ins_idx = stacks[to].len();
    for _ in 0..count {
        if let Some(val) = stacks[from].pop() {
            stacks[to].insert(ins_idx, val);
        } else {
            stacks[to].insert(ins_idx, b'?');
        }
    }
}

fn main() {
    let mut inp = String::new();
    let mut stacks: Vec<Vec<u8>> = vec![vec![]; 10];
    // stacks = Vec::new();
    // for _ in 0 .. 10 {
    //   stacks.push(Vec::new::<u8>());
    // }
    // Read Stacks
    loop {
        inp.clear();
        io::stdin().read_line(&mut inp).expect("cannot read line");
        println!("{}", inp);
        if !add_to_stacks(&mut stacks, &inp) {
            break;
        }
    }
    show_stacks(&stacks);
    // read blank line
    io::stdin().read_line(&mut inp).expect("cannot read line");
    // read moves
    loop {
        inp.clear();
        let bytes = io::stdin().read_line(&mut inp).expect("cannot read line");
        if bytes == 0 {
            break;
        }
        let line = inp.trim();
        let (count, from, to) = parse_move(&line);
        println!("count {count}  from {from} to {to}");
        move_stacks2(&mut stacks, count, from, to);
        show_stacks(&stacks);
    }
    println!("{}", build_solution(&stacks));
}
