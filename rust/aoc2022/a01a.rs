use std::io;

fn main() {
    let mut elves = Vec::new();
    loop {
        let elf = read_elf();
        if elf < 0 {
            break;
        }
        elves.push(elf);
    }
    println!("{elves:?}");
    let cal_max = get_max(&elves);
    println!("Max: {cal_max}");
    println!("{elves:?}");
}

fn read_elf() -> i32 {
    let mut inp = String::new();
    let mut sum = 0;
    loop {
        inp.clear();
        let bytes = io::stdin().read_line(&mut inp).expect("not readable");
        if bytes == 0 {
            if sum > 0 {
                return sum;
            } else {
                return -1;
            }
        }
        let inp = inp.trim();
        if inp == "" {
            return sum;
        }
        sum += inp.parse::<i32>().expect("not a number");
    }
}

fn get_max(elves: &Vec<i32>) -> i32 {
    let mut max = elves[0];
    for elf in elves {
        if *elf > max {
            max = *elf;
        }
    }
    max
}
