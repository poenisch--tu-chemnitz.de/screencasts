/// Verbesserte Suche: Binäre Suche
/// Idee:
/// Suche 23 (20)
/// Idx: 0  1  2  3  4  5  6  7  8
/// Val: 1  3 11 15 18 23 24 27 31
///                  ^  5  6  7  8
///                    23 24 27 31
///                        ^
///                     5
///                    23
/// Voraussetzung: Feld ist sortiert!
mod simple_input;
use simple_input::Input;

const MAX_SIZE: usize = 30;

fn show_numbers(numbers: &[i32; MAX_SIZE], size: usize) {
    print!("Idx:");
    for idx in 0..size {
        print!("{:3}", idx);
    }
    println!();
    print!("Val:");
    for idx in 0..size {
        print!("{:3}", numbers[idx]);
    }
    println!();
}

// Sucht val im Feld
// wenn nicht gefunden, Rückgabe von size (numbers[size-1] ist letztes Element)
fn lin_search(numbers: &[i32; MAX_SIZE], size: usize, val: i32) -> usize {
    for idx in 0..size {
        if numbers[idx] == val {
            return idx;
        }
    }
    size
}

// Prüfen der Sortierung
fn is_sorted(numbers: &[i32; MAX_SIZE], size: usize) -> bool {
    for idx in 1..size {
        // Vergleich mit Vorgänger
        if numbers[idx - 1] > numbers[idx] {
            return false;
        }
    }
    true
}

/// Virtuell: numbers[-1] = -∞, numbers[size] = ∞
/// Invariante: numbers[left-1] < val ≤ numbers[right]
/// Erstaufruf: left = 0, right = size
fn bin_search_rec(numbers: &[i32; MAX_SIZE], left: usize, right: usize, val: i32) -> usize {
    if left == right {
        return right;
    }
    let m = (left + right) / 2;
    print!("print_search_rec l: {left} r: {right} m: {m}");
    println!(" numbers[m]: {} (search {})", numbers[m], val);
    if numbers[m] < val {
        bin_search_rec(numbers, m + 1, right, val)
    } else {
        bin_search_rec(numbers, left, m, val)
    }
}

/// Vor- und Nachbereitung Binäre Suche
fn bin_search(numbers: &[i32; MAX_SIZE], size: usize, val: i32) -> usize {
    let pos = bin_search_rec(numbers, 0, size, val);
    // Wert tatsächlich gefunden?
    // Invariante: numbers[pos] ≥ val und numbers[pos-1] < val
    if pos < size && numbers[pos] == val {
        pos
    } else {
        size
    }
}

fn main() {
    let mut numbers = [0; MAX_SIZE];
    let mut size: usize = 0;
    // Eingabe
    let mut inp = Input::new();
    let mut val = inp.read_int();
    while val >= 0 {
        numbers[size] = val;
        size += 1;
        val = inp.read_int();
    }
    //println!("Size: {} Content: {:?}", size, numbers);
    show_numbers(&numbers, size);
    let sorted = is_sorted(&numbers, size);
    println!("Sortiert: {sorted}");
    // Suche
    val = inp.read_int();
    while val >= 0 {
        let pos;
        if sorted {
            // binäre Suche
            pos = bin_search(&numbers, size, val);
        } else {
            // lineare Suche
            pos = lin_search(&numbers, size, val);
        }
        if pos < size {
            println!("value {} at pos {}", val, pos);
        } else {
            println!("value {} not found", val);
        }
        val = inp.read_int();
    }
}
