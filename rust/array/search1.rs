mod simple_input;
use simple_input::Input;

const MAX_SIZE: usize = 30;

fn show_numbers(numbers: &[i32; MAX_SIZE], size: usize) {
    print!("Idx:");
    for idx in 0..size {
        print!("{:3}", idx);
    }
    println!();
    print!("Val:");
    for idx in 0..size {
        print!("{:3}", numbers[idx]);
    }
    println!();
}

// Sucht val im Feld
// wenn nicht gefunden, Rückgabe von size (numbers[size-1] ist letztes Element)
fn search_number(numbers: &[i32; MAX_SIZE], size: usize, val: i32) -> usize {
    for idx in 0..size {
        if numbers[idx] == val {
            return idx;
        }
    }
    size
}

fn main() {
    let mut numbers = [0; MAX_SIZE];
    let mut size: usize = 0;
    // Eingabe
    let mut inp = Input::new();
    let mut val = inp.read_int();
    while val >= 0 {
        numbers[size] = val;
        size += 1;
        val = inp.read_int();
    }
    //println!("Size: {} Content: {:?}", size, numbers);
    show_numbers(&numbers, size);
    // Suche
    val = inp.read_int();
    while val >= 0 {
        let pos = search_number(&numbers, size, val);
        if pos < size {
            println!("value {} at pos {}", val, pos);
        } else {
            println!("value {} not found", val);
        }
        val = inp.read_int();
    }
}
