/// Polynom 2. Grades
/// f(x) = ax² + bx + c
///      = a₂x² + a₁x + a₀
/// Aufruf ./poly1 x a₂ a₁ a₀
mod simple_args;

fn main() {
    let args = simple_args::int_args();
    println!("Anzahl Argumente: {}", args.len());
    let x = args[0];
    let a_2 = args[1];
    let a_1 = args[2];
    let a_0 = args[3];
    println!("Polynom: {}x² + {}x + {}, x = {}", a_2, a_1, a_0, x);
    let y = a_2 * x * x + a_1 * x + a_0;
    println!("p({}) = {}", x, y);
}
