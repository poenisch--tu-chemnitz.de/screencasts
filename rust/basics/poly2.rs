/// Polynom 3. Grades
/// f(x) = dx³ + ax² + bx + c
///      = a₃x³ + a₂x² + a₁x + a₀
/// Aufruf ./poly1 x a₃ a₂ a₁ a₀
///
///    a₃ x³ + a₂ x² + a₁x + a₀
/// = (a₃x² + a₂x + a₁)x + a₀
/// = (a₃x + a₂)x + a₁)x + a₀
/// Berechnung (Horner-Schema):
/// a₃
///    * x
///        + a₂
///             * x
///                 + a₁
///                      * x
///                          + a₀
mod simple_args;

fn main() {
    let args = simple_args::int_args();
    println!("Anzahl Argumente: {}", args.len());
    let x = args[0];
    let a_3 = args[1];
    let a_2 = args[2];
    let a_1 = args[3];
    let a_0 = args[4];
    println!(
        "Polynom: {}x³ + {}x² + {}x + {}, x = {}",
        a_3, a_2, a_1, a_0, x
    );
    let mut y = 0;
    y += a_3;
    y *= x;
    y += a_2;
    y *= x;
    y += a_1;
    y *= x;
    y += a_0;
    println!("p({}) = {}", x, y);
}
