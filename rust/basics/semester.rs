// Lernaufwand im Semester

mod simple_input;
use simple_input::Input;

const LECTURE_MINS: i32 = 45;
const MIN_HOUR: i32 = 60;
const SEM_WEEKS: i32 = 15;

fn main() {
    let mut inp = Input::new();
    println!("Vorlesungsstunden pro Woche:");
    let lectures = inp.read_int();
    println!("Aufwand Selbststudium (%):");
    let percent_self_study = inp.read_int();
    println!(
        "Vorlesungsstunden: {} Selbststudium {} %",
        lectures, percent_self_study
    );
    let lecture_minutes = lectures * LECTURE_MINS;
    println!("Dauer LVs: {lecture_minutes} min");
    println!(
        "  = {} h {} min",
        lecture_minutes / MIN_HOUR,
        lecture_minutes % MIN_HOUR
    );
    let self_study_minutes = lecture_minutes * percent_self_study / 100;
    println!("Dauer Selbststudium: {self_study_minutes} min");
    println!(
        "  = {} h {} min",
        self_study_minutes / MIN_HOUR,
        self_study_minutes % MIN_HOUR
    );
    let total_study_minutes = lecture_minutes + self_study_minutes;
    println!("Aufwand pro Woche: {total_study_minutes} min");
    println!(
        "  = {} h {} min",
        total_study_minutes / MIN_HOUR,
        total_study_minutes % MIN_HOUR
    );
    let total_sem_minutes = total_study_minutes * SEM_WEEKS;
    println!("Aufwand pro Semester: {total_sem_minutes} min");
    println!(
        "  = {} h {} min",
        total_sem_minutes / MIN_HOUR,
        total_sem_minutes % MIN_HOUR
    );
}
