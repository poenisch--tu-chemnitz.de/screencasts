/// Bestimmen, ob Datum im Semester
/// ./semester1 monat tag
/// WS: 1.10.-15.2.
/// SS: 1.4.-15.7.
mod simple_args;

// Einfache Prüfung
fn is_ss(month: i32) -> bool {
    month >= 4 && month <= 7
}

#[test]
fn test_is_ss() {
    assert_eq!(is_ss(4), true);
    assert_eq!(is_ss(7), true);
    assert_eq!(is_ss(8), false);
}

fn is_ws(month: i32) -> bool {
    (month >= 10 && month <= 12) || (month >= 1 && month <= 2)
}

#[test]
fn test_is_ws() {
    assert_eq!(is_ws(10), true);
    assert_eq!(is_ws(12), true);
    assert_eq!(is_ws(13), false);
    assert_eq!(is_ws(9), false);
    assert_eq!(is_ws(3), false);
}

fn is_semester(month: i32) -> bool {
    is_ss(month) || is_ws(month)
}

#[test]
fn test_is_semester() {
    assert_eq!(is_semester(10), true);
}

// Exakte Prüfung
fn is_ss_exact(month: i32, day: i32) -> bool {
    (month >= 4 && month <= 6) || (month == 7 && day >= 1 && day <= 15)
}

#[test]
fn test_ss_exact() {
    assert_eq!(is_ss_exact(7, 1), true);
    assert_eq!(is_ss_exact(7, 16), false);
}

fn is_ws_exact(month: i32, day: i32) -> bool {
    (month >= 10 && month <= 12) || month == 1 || (month == 2 && day >= 1 && day <= 15)
}

fn is_semester_exact(month: i32, day: i32) -> bool {
    is_ss_exact(month, day) || is_ws_exact(month, day)
}

// Datumsprüfung
fn is_date_valid(month: i32, day: i32) -> bool {
    month >= 1 && month <= 12 && day >= 1 && day <= 31
}

fn main() {
    let args = simple_args::int_args();
    if args.len() != 2 {
        simple_args::usage("monat tag");
        return;
    }
    let month = args[0];
    let day = args[1];
    println!("Tag: {day:02}.{month:02}.");
    if !is_date_valid(month, day) {
        println!("Datum ungültig");
        return;
    }
    println!("Einfach:");
    if is_semester(month) {
        println!("im Semester");
    } else {
        println!("nicht im Semester");
    }
    println!("Exakt:");
    if is_semester_exact(month, day) {
        println!("im Semester");
    } else {
        println!("nicht im Semester");
    }
}
