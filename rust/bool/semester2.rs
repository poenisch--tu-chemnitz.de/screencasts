/// Bestimmen, ob Datum im Semester
/// ./semester1 monat tag
/// WS: 1.10.-15.2.
/// SS: 1.4.-15.7.
mod simple_args;

// Mehrfachverzweigung:
// month:
// if
// + 1: true
// - if
//   + 2: prüfe Tag -> { true, false}
//   - if
//     + 4, 5, 6 ->  true
//     - if
//       + 7: prüfe Tag -> { true, false }
//       - ...

fn is_semester1(month: i32, day: i32) -> bool {
    if month == 1 {
        true
    } else if month == 2 {
        day >= 1 && day <= 15
    } else if month >= 4 && month <= 6 {
        true
    } else if month == 7 {
        day >= 1 && day <= 15
    } else if month >= 10 && month < 12 {
        true
    } else {
        // 3, 8, 9
        false
    } // if hat Wert, der zurückgegeben wird
}

fn is_semester2(month: i32, day: i32) -> bool {
    match month {
        // WS
        10 | 11 | 12 => true,
        1 => true,
        2 => {
            println!("{}. {}.", day, month);
            day >= 1 && day <= 15
        }
        // Pause
        3 => false,
        // SS
        4 | 5 | 6 => true,
        7 => day >= 1 && day <= 15,
        // Pause
        8 | 9 => false,
        // nicht möglich
        _ => false,
    }
}

// Datumsprüfung
fn is_date_valid(month: i32, day: i32) -> bool {
    month >= 1 && month <= 12 && day >= 1 && day <= 31
}

fn main() {
    let args = simple_args::int_args();
    if args.len() != 2 {
        simple_args::usage("monat tag");
        return;
    }
    let month = args[0];
    let day = args[1];
    println!("Tag: {day:02}.{month:02}.");
    if !is_date_valid(month, day) {
        println!("Datum ungültig");
        return;
    }
    println!("if-elseif-Kette:");
    if is_semester1(month, day) {
        println!("im Semester");
    } else {
        println!("nicht im Semester");
    }
    println!("match-Ausdruck:");
    if is_semester2(month, day) {
        println!("im Semester");
    } else {
        println!("nicht im Semester");
    }
}
