/// Test Wurzelberechnung mit Bisektionsverfahren
mod simple_args;

/// Hilfsfunktion Quadrat
fn sqr(x: i32) -> i32 {
    x * x
}

#[test]
fn test_sqr() {
    assert_eq!(sqr(3), 9);
    assert_eq!(sqr(-2), 4);
    assert_eq!(sqr(0), 0);
    assert_eq!(sqr(1), 1);
}

// Demo: Test auch ausgeführt, wenn voriger abbricht
//#[test]
//fn test_sqr2() {
//    assert_eq!(sqr(3), -9);
//    assert_eq!(sqr(-2), 4);
//}

/// Forderung an res: t_left² <= val < t_right²
fn try_sqrt(val: i32, t_left: i32, t_right: i32) -> i32 {
    assert!(sqr(t_left) <= val, "ass. {}² ≤ {} failed", t_left, val);
    assert!(sqr(t_right) > val, "ass. {}² > {} failed", t_right, val);
    // TODO: auch t_left ≥ 0 und val ≥ 0 sicherstellen
    if t_right - t_left == 1 {
        // Abbruch
        return t_left;
    }
    let mean = (t_left + t_right) / 2;
    if sqr(mean) > val {
        // ersetze t_right
        return try_sqrt(val, t_left, mean);
    } else {
        // ersetze t_left
        return try_sqrt(val, mean, t_right);
    }
}

#[test]
fn test_try_sqrt() {
    //assert_eq!(try_sqrt(42, 3, 4), 3); // Fehler durch Assertion
    assert_eq!(try_sqrt(42, 6, 7), 6); // Test Abstand
    assert_eq!(try_sqrt(0, 0, 1), 0);
    assert_eq!(try_sqrt(1, 0, 2), 1);
    assert_eq!(try_sqrt(9, 3, 5), 3);
    assert_eq!(try_sqrt(9, 0, 5), 3);
    assert_eq!(try_sqrt(10, 2, 20), 3);
    assert_eq!(try_sqrt(15, 2, 20), 3);
    //assert_ne!(try_sqrt(15, 4, 16), 3); // fehlerhafter Aufruf
}

/// Wurzel ganzzahlig
fn sqrt(val: i32) -> i32 {
    if val < 0 {
        return -1;
    }
    let res = try_sqrt(val, 0, val + 1);
    let err_left = val - sqr(res);
    let err_right = sqr(res + 1) - val;
    if err_left < err_right {
        return res;
    } else {
        return res + 1;
    }
}

#[test]
fn test_sqrt() {
    assert_eq!(sqrt(4), 2);
    assert_eq!(sqrt(0), 0);
    assert_eq!(sqrt(1), 1);
    assert_eq!(sqrt(-42), -1);
    assert_eq!(sqrt(10), 3);
    assert_eq!(sqrt(15), 4);
}

/// rekursive Ausgabe aller Wurzeln von val bis val_max
fn show_all_sqrts(val: i32, max_val: i32) {
    // Abbruchbedingung
    if val > max_val {
        return;
    }
    let res = sqrt(val);
    // Probe:
    let err = sqr(res) - val;
    println!("√{} = {}, error = {}", val, res, err);
    show_all_sqrts(val + 1, max_val);
}

fn main() {
    let args = simple_args::int_args();
    if args.len() != 1 {
        simple_args::usage("max_arg");
        return;
    }
    let max_val = args[0];
    show_all_sqrts(0, max_val);
    // nur Einzeltest
    // let res = sqrt(max_val);
    // let err = sqr(res) - max_val;
    // println!("√{} = {}, error = {}", max_val, res, err);
}
