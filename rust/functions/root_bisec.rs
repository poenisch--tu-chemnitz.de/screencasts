/// Wurzelberechnung mit Bisektionsverfahren
// Vorgehen:
// √80
// t_left t_right  t = mean(t_left, t_right) t²
// 0      81       40                        1600
// 0      40       20                         400
// 0      20       10                         100
// 0      10        5                          25 (≤ 80)
// 5      10        7                          49 (≤ 80)
// 7      10        8                          64 (≤ 80)
// 8      10        9                          81 (> 80)
// 8       9 → fertig √80 liegt zwischen 8 und 9

mod simple_args;

/// Hilfsfunktion Quadrat
fn sqr(x: i32) -> i32 {
    x * x
}

/// Forderung an res: t_left² <= val < t_right²
fn try_sqrt(val: i32, t_left: i32, t_right: i32) -> i32 {
    if t_right - t_left == 1 { // Abbruch
        return t_left;
    }
    let mean = (t_left + t_right) / 2;
    if sqr(mean) > val {
        // ersetze t_right
        return try_sqrt(val, t_left, mean);
    } else {
        // ersetze t_left
        return try_sqrt(val, mean, t_right);
    }
}

/// Wurzel ganzzahlig
fn sqrt(val: i32) -> i32 {
    if val < 0 {
        return -1;
    }
    let res = try_sqrt(val, 0, val + 1);
    let err_left = val - sqr(res);
    let err_right = sqr(res + 1) - val;
    if err_left < err_right {
        return res;
    } else {
        return res + 1;
    }
}

/// rekursive Ausgabe aller Wurzeln von val bis val_max
fn show_all_sqrts(val: i32, max_val: i32) {
    // Abbruchbedingung
    if val > max_val {
        return;
    }
    let res = sqrt(val);
    // Probe:
    let err = sqr(res) - val;
    println!("√{} = {}, error = {}", val, res, err);
    show_all_sqrts(val + 1, max_val);
}

fn main() {
    let args = simple_args::int_args();
    if args.len() != 1 {
        simple_args::usage("max_arg");
        return;
    }
    let max_val = args[0];
    show_all_sqrts(0, max_val);
    // nur Einzeltest
    // let res = sqrt(max_val);
    // let err = sqr(res) - max_val;
    // println!("√{} = {}, error = {}", max_val, res, err);
}
