/// Polynom 3. Grades mit Funktionen
mod simple_args;

fn poly_3(x: i32, a_3: i32, a_2: i32, a_1: i32, a_0: i32) -> i32 {
    let mut y = 0;
    y += a_3;
    y *= x;
    y += a_2;
    y *= x;
    y += a_1;
    y *= x;
    y += a_0;
    y
}

fn pr_poly(x: i32, x_max: i32, a_3: i32, a_2: i32, a_1: i32, a_0: i32) {
    if x > x_max {
        return;
    }
    let y = poly_3(x, a_3, a_2, a_1, a_0);
    println!("{x_min:2} {y:5}");
    pr_poly(x + 1, x_max, a_3, a_2, a_1, a_0);
}

fn usage() {
    println!("Usage: ./poly x_max a_3 a_2 a_1 a_0");
    println!("Computes polynomial of degree 3 from x=0 to x=x_max");
}

fn main() {
    let args = simple_args::int_args();
    if args.len() != 5 {
        usage();
        return;
    }
    let x_max = args[0];
    let a_3 = args[1];
    let a_2 = args[2];
    let a_1 = args[3];
    let a_0 = args[4];
    println!(
        "#p(x) = {}x³ + {}x² + {}x + {}, x_max = {}",
        a_3, a_2, a_1, a_0, x_max
    );
    println!("#x     y");
    pr_poly(0, x_max, a_3, a_2, a_1, a_0);
}
