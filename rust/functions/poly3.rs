/// Polynom 3. Grades mit Funktionen
mod simple_args;

fn poly_3(x: i32, a_3: i32, a_2: i32, a_1: i32, a_0: i32) -> i32 {
    let mut y = 0;
    y += a_3;
    y *= x;
    y += a_2;
    y *= x;
    y += a_1;
    y *= x;
    y += a_0;
    y
}

fn usage() {
    println!("Usage: ./poly x a_3 a_2 a_1 a_0");
    println!("Computes polynomial of degree 3");
}

fn main() {
    let args = simple_args::int_args();
    if args.len() != 5 {
        usage();
        return;
    }
    let x = args[0];
    let a_3 = args[1];
    let a_2 = args[2];
    let a_1 = args[3];
    let a_0 = args[4];
    println!(
        "p(x) = {}x³ + {}x² + {}x + {}, x = {}",
        a_3, a_2, a_1, a_0, x
    );
    let y = poly_3(x, a_3, a_2, a_1, a_0);
    println!("p({}) = {}", x, y);
}
