/// Einfache Wurzelberechnung
// Wurzel: Compose v /

mod simple_args;

/// Hilfsfunktion Quadrat
fn sqr(x: i32) -> i32 {
    x * x
}

/// Forderung an res: res² <= val < (res+1)²
fn try_sqrt(val: i32, test: i32) -> i32 {
    // erfüllt: test² <= val
    if sqr(test + 1) > val {
        // test + 1 ist größer als √val
        return test;
    }
    return try_sqrt(val, test + 1);
}

/// Wurzel ganzzahlig
fn sqrt(val: i32) -> i32 {
    if val < 0 {
        return -1;
    }
    let res = try_sqrt(val, 0);
    let err_left = val - sqr(res);
    let err_right = sqr(res + 1) - val;
    if err_left < err_right {
        return res;
    } else {
        return res + 1;
    }
}

/// rekursive Ausgabe aller Wurzeln von val bis val_max
fn show_all_sqrts(val: i32, max_val: i32) {
    // Abbruchbedingung
    if val > max_val {
        return;
    }
    let res = sqrt(val);
    // Probe:
    let err = sqr(res) - val;
    println!("√{} = {}, error = {}", val, res, err);
    show_all_sqrts(val + 1, max_val);
}

fn main() {
    let args = simple_args::int_args();
    if args.len() != 1 {
        simple_args::usage("max_arg");
        return;
    }
    let max_val = args[0];
    show_all_sqrts(0, max_val);
}
