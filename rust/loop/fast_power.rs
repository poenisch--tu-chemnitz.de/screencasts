mod simple_args;

fn main() {
    let args = simple_args::int_args();
    if args.len() != 3 {
        simple_args::usage("base exponent module");
        return;
    }
    let a = args[0];
    let n = args[1];
    let m = args[2];
    let (res1, cnt1) = power(a, n, m);
    println!("{}^{} = {} mod {} in {} operations", a, n, res1, m, cnt1);
    let (res2, cnt2) = fast_power(a, n, m);
    println!("{}^{} = {} mod {} in {} operations", a, n, res2, m, cnt2);
}

fn power(a: i32, n: i32, m: i32) -> (i32, i32) {
    let mut res = 1;
    let mut op_cnt = 0;
    for _ in 0..n {
        res = (res * a) % m;
        op_cnt += 2;
    }
    (res, op_cnt)
}

// 2^13 = 2^(8 + 4 + 1) = 2^8 * 2^4 * 2^1
// Binärdarstellung von 13: 1*8 + 1*4 + 0*2 + 1*1
// 2^2 = (2^1)^2, 2^4 = (2^2)^2, 2^8 = (2^4)^2
//
// Exp. %2  Quad         Prod
// 13    1     2  2^1       2
//  6    0     4  2^2       -
//  3    1    16  2^4      16
//  1    1   256  2^8     256
//  0    0     -            -
//                      -----
//        		 8192

fn fast_power(a: i32, mut n: i32, m: i32) -> (i32, i32) {
    let mut res = 1;
    let mut q = a;
    let mut op_cnt = 0;
    while n > 0 {
        if n % 2 == 1 {
            res = (res * q) % m;
            op_cnt += 2
        }
        op_cnt += 1;
        q = (q * q) % m;
        op_cnt += 2;
        n /= 2;
        op_cnt += 1;
    }
    (res, op_cnt)
}
