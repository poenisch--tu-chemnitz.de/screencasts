/// Zinsberechnung
mod simple_args;

/// Zinsberechnung
/// Ergebnis: neues Guthaben
#[allow(dead_code)]
fn compute_interest_rec(balance: i32, rate: i32, duration: i32) -> i32 {
    if duration == 0 {
	return balance;
    } else {
	// duration-mal ausführen:
	let balance_new = balance + balance * rate /100;
	println!("- Guthaben {}", balance_new);
	return compute_interest_rec(balance_new, rate, duration-1);
    }
}

#[allow(dead_code)]
fn compute_interest1(mut balance: i32, rate: i32, duration: i32) -> i32 {
    let mut year = 0;
    while year < duration {
	year += 1;
	// duration-mal ausführen:
	balance = balance + balance * rate /100;
	println!("Jahr: {:2} Guthaben: {:8}", year, balance);
    }
    balance
}

fn compute_interest2(mut balance: i32, rate: i32, duration: i32) -> i32 {
    for year in 1 ..= duration {
	// duration-mal ausführen:
	balance = balance + balance * rate /100;
	println!("J. {:2} Guthaben: {:8}", year, balance);
    }
    balance
}

fn dur_double(mut balance: i32, rate: i32) -> i32 {
    let mut year = 0;
    let balance_double = 2 * balance;
    while balance < balance_double {
	year += 1;
	// duration-mal ausführen:
	balance = balance + balance * rate /100;
	println!("Jahr: {:2} Guthaben: {:8}", year, balance);
    }
    year
}

fn main() {
    let args = simple_args::int_args();
    if args.len() != 2 {
	simple_args::usage("guthaben zinsen_prozent");
	return;
    }
    let duration = 10;
    let balance = args[0];
    let rate = args[1];
    println!("Startguthaben: {}, Zinssatz: {}%, Dauer: {} Jahre",
      balance, rate, duration);
    let new_balance = compute_interest2(balance, rate, duration);
    println!("Guthaben nach {} Jahren: {}", duration, new_balance);
    let duration2 = dur_double(balance, rate);
    println!("Verdoppelung nach {} Jahren.", duration2);
}
